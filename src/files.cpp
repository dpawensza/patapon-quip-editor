#include <QFileDialog>
#include <QSaveFile>
#include <QTemporaryFile>

#include "constants.hpp"
#include "error.hpp"
#include "files.hpp"
#include <libP3Hash.cpp>

namespace pqe {

QString execPathDialog(QWidget *parent) {
  return QFileDialog::getOpenFileName(parent, "Select file", QString(),
                                      "Patapon 3 savedata (*.BIN)");
}

std::optional<QByteArray> loadData(const QString &path, bool pspEncrypted,
                                   bool p3Encrypted) {
  QFile file(path);
  if (!file.open(QFile::ReadOnly)) {
    error("Could not read input file!");
    return std::nullopt;
  }
  QByteArray data = file.readAll();
  if (p3Encrypted) {
    data = decryptData(data);
  }
  if (pspEncrypted) {
  } // not implemented
  file.close();
  if (data.size() != SAVE_SIZE) {
    error("File size invalid! Is the data additionally encrypted?");
    return std::nullopt;
  }
  return data;
}

bool saveData(const QString &path, QByteArray newData, bool backup,
              bool pspEncrypt, bool p3Encrypt) {
  QSaveFile file(path);
  if (!file.open(QFile::WriteOnly)) {
    error("Could not open output file for writing!");
    return false;
  }
  if (backup) {
    // Remove old backup file, if exists
    QFile::remove(path + QString(".BAK"));
    // Copy current file to backup path before saving
    QFile::copy(path, path + QString(".BAK"));
  }

  // Increment edit counter
  newData[0x10] += 1;

  if (p3Encrypt) {
    newData = encryptData(newData);
  }
  if (pspEncrypt) {
  } // not implemented
  if (qint64 bytesWritten;
      (bytesWritten = file.write(newData)) != newData.size()) {
    if (bytesWritten == -1) {
      file.cancelWriting();
      error("An error occured while attempting to write data (-1 returned). No "
            "data was written");
      return false;
    } else /* 0 <= bytesWritten < newData.size() */ {
      file.cancelWriting();
      error("Data write attempt failed (partial write; " +
            QString::number(bytesWritten) + " returned). No data was written");
      return false;
    }
  }
  file.commit();
  return true;
}

QString loadQuip(const QByteArray &data, qint32 address) {
  // Not a custom quip
  if (!(data.at(address) == (char)0xFF && data.at(address + 1) == (char)0xFF &&
        data.at(address + 2) == (char)0xFF &&
        data.at(address + 3) == (char)0xFF))
    return QString("");

  QString quip;
  for (int i = 4; i < 0x24; i += 2) {
    // Assuming the file is little-endian
    qint16 code = (data.at(address + i + 1) << 8) + data.at(address + i);
    if (code == 0)
      break;
    QChar chr(code);
    quip.append(chr);
  }

  return quip;
}

void writeQuip(QByteArray &data, qint32 address, const QString &quip) {
  assert(address + 0x27 < data.size());

  // Quips cannot be empty in base game
  if (quip.isEmpty())
    return;

  data[address] = (char)0xFF;
  data[address + 1] = (char)0xFF;
  data[address + 2] = (char)0xFF;
  data[address + 3] = (char)0xFF;

  for (int i = 4; i < 0x24; i += 2) {
    if ((i - 4) / 2 >= quip.size()) {
      data[address + i] = 0x0;
      data[address + i + 1] = 0x0;
      break;
    }

    qint16 code = quip.at((i - 4) / 2).unicode();
    data[address + i + 1] = (code >> 8);
    data[address + i] = code & 0xFF;
  }
}

QByteArray decryptData(const QByteArray &data) {
  QTemporaryFile wtempfile, rtempfile;
  if (!rtempfile.open() || !wtempfile.open())
    return {};
  rtempfile.write(data);
  libP3Hash::decryptFile(rtempfile.fileName().toStdString(),
                         wtempfile.fileName().toStdString());
  return wtempfile.readAll();
}

QByteArray encryptData(const QByteArray &data) {
  QTemporaryFile wtempfile, rtempfile;
  if (!rtempfile.open() || !wtempfile.open())
    return {};
  rtempfile.write(data);
  libP3Hash::encryptFile(rtempfile.fileName().toStdString(),
                         wtempfile.fileName().toStdString());
  return wtempfile.readAll();
}

} // namespace pqe

// p3qe
// Copyright (C) 2023-2025 jan Mikowa
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
