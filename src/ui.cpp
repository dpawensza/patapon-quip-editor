#include "ui.hpp"
#include "constants.hpp"
#include "files.hpp"

namespace pqe {

namespace forms {

MainForm::MainForm(QWidget *parent, GuiWrapperApp &parentApp)
    : QWidget(parent), _parentWidget(parent), _parentApp(parentApp) {
  _ui.setupUi(_parentWidget);
  connect(_ui.loadBtn, &QPushButton::clicked, this, &MainForm::_loadData);
  connect(_ui.saveBtn, &QPushButton::clicked, this, &MainForm::_saveData);
  connect(_ui.aboutBtn, &QPushButton::clicked, this, &MainForm::_showAbout);
  connect(_ui.modifiersBtn, &QPushButton::clicked, this,
          &MainForm::_showModifiers);
  _ui.credLabel->setText(QString("p3qe ") + QString(VERSION) +
                         QString(" by pear"));
}

void MainForm::_loadData() {
  _parentApp._filePath = execPathDialog(_parentWidget);
  if (_parentApp._filePath.isEmpty())
    return;

  std::optional<QByteArray> load_result =
      loadData(_parentApp._filePath, false, _ui.p3Check->isChecked());

  // Something went wrong inside of loadData()
  if (!load_result.has_value())
    return;
  else
    _parentApp._fileData = *load_result;

  _ui.statusLabel->setText(QString("Loaded file: ") + _parentApp._filePath);

  enableStuff();
  fillData();
}

void MainForm::_saveData() {
  writeData();

  bool result =
      saveData(_parentApp._filePath, _parentApp._fileData,
               _ui.bkCheck->isChecked(), false, _ui.p3eCheck->isChecked());

  if (result)
    _ui.statusLabel->setText(QString("Saved to file: ") + _parentApp._filePath);
}

void MainForm::writeData() {
  // set quip type radio button
  if (_ui.mode4RBtn->isChecked()) {
    _parentApp._fileData[0x8A8] = QuipType::FOUR_QUIPS;
  } else if (_ui.mode9RBtn->isChecked()) {
    _parentApp._fileData[0x8A8] = QuipType::NINE_QUIPS;
  } else if (_ui.mode24RBtn->isChecked()) {
    _parentApp._fileData[0x8A8] = QuipType::HIDEOUT;
  } else {
    // ??? it's a radio button, this shouldn't happen
  }

  // general-use lambda for code deduplication
  const auto write_impl = [this](QWidget *tab, qint32 addressOffset) {
    int i = 0;
    for (QLineEdit *lineEdit : tab->findChildren<QLineEdit *>()) {
      qint32 quipAddress = addressOffset + i * 0x28;
      writeQuip(_parentApp._fileData, quipAddress, lineEdit->text());
      ++i;
    }
  };

  // write tabs
  {
    write_impl(_ui.chat4Tab, 0x23C);
    write_impl(_ui.chat9Tab, 0x0D4);
    write_impl(_ui.chat24Tab, 0x4E4);
    write_impl(_ui.autochatTab, 0x3A4);
  }
}

void MainForm::enableStuff() {
  _ui.saveBtn->setEnabled(true);
  for (QRadioButton *radioBtn : _ui.modeGroup->findChildren<QRadioButton *>())
    radioBtn->setCheckable(true);
  for (QObject *quipTab : _ui.quipTabs->children())
    for (QLineEdit *quipEdit : quipTab->findChildren<QLineEdit *>()) {
      quipEdit->setReadOnly(false);
      quipEdit->setMaxLength(16);
    }
}

void MainForm::fillData() {
  // load quip type radio button
  switch (_parentApp._fileData.at(0x8A8)) {
  case QuipType::FOUR_QUIPS:
    _ui.mode4RBtn->setChecked(true);
    break;
  case QuipType::NINE_QUIPS:
    _ui.mode9RBtn->setChecked(true);
    break;
  case QuipType::HIDEOUT:
    _ui.mode24RBtn->setChecked(true);
    break;
  }

  // general-use lambda for code deduplication
  const auto load_impl = [this](QWidget *tab, qint32 addressOffset) {
    int i = 0;
    for (QLineEdit *lineEdit : tab->findChildren<QLineEdit *>()) {
      qint32 quipAddress = addressOffset + i * 0x28;
      QString quipText = loadQuip(_parentApp._fileData, quipAddress);
      lineEdit->setText(quipText);
      ++i;
    }
  };

  // load tabs
  {
    load_impl(_ui.chat4Tab, 0x23C);
    load_impl(_ui.chat9Tab, 0x0D4);
    load_impl(_ui.chat24Tab, 0x4E4);
    load_impl(_ui.autochatTab, 0x3A4);
  }
}

void MainForm::_showAbout() {
  AboutForm *about = new AboutForm(_parentWidget);
  about->exec();
  delete about;
}

AboutForm::AboutForm(QWidget *parent) : _parent(parent) {
  _ui.setupUi(this);
  _ui.verLabel->setText(VERSION);
  this->setWindowTitle("About");
}

void MainForm::_showModifiers() {
  ModifiersForm *modifiers = new ModifiersForm(_parentWidget);
  modifiers->exec();
  delete modifiers;
}

ModifiersForm::ModifiersForm(QWidget *parent) : _parent(parent) {
  _ui.setupUi(this);
  this->setWindowTitle("Modifiers");
}

} // namespace forms

GuiWrapperApp::GuiWrapperApp()
    : _widget(new QWidget()), _mainForm(new forms::MainForm(_widget, *this)) {}

GuiWrapperApp::~GuiWrapperApp() {
  delete _mainForm;
  delete _widget;
}

void GuiWrapperApp::show() {
  _widget->setWindowTitle("");
  _widget->show();
}

} // namespace pqe

// p3qe
// Copyright (C) 2023-2025 jan Mikowa
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
