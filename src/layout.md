# Save file layout documentation

**Credit: [Mielikki](https://github.com/kanelikki)**

## Quips

Each quip is 0x28 bytes:
| **Part of quip**                           | **Address range** |
|--------------------------------------------|:-----------------:|
| Message type (0xFFFFFFFF = custom message) |    0x00 - 0x03    |
| Message content                            |    0x04 - 0x28    |

Quips are organised into blocks, placed at the following addresses:
| **Block name**                                           | **Address range** |
|----------------------------------------------------------|:-----------------:|
| Easy-type quips (4 quips in quest)                       |   0x23C - 0x2DB   |
| Quest-type quips (9 quips in quest)                      |   0x0D4 - 0x23B   |
| Hideout-style quips (24 quips in quest)                  |   0x4E4 - 0x8A3   |
| Autochat (quips appear in the same order as in the game) |   0x3A4 - 0x4BB   |

The quip type setting (4/9/24 quips) is located at 0x8A8 and has the following values:
| **Quip type**            | **Value** |
|--------------------------|:---------:|
| Easy-type (4 quips)      |    0x0    |
| Quest-type (9 quips)     |    0x1    |
| Hideout-style (24 quips) |    0x2    |
