#pragma once

#include <QWidget>

#include "ui_about.h"
#include "ui_mainwidget.h"
#include "ui_modifiers.h"

namespace pqe {

class GuiWrapperApp;

namespace forms {

class MainForm : public QWidget {
  Q_OBJECT

public:
  explicit MainForm(QWidget *parent, GuiWrapperApp &parentApp);

private slots:
  void _loadData();
  void _saveData();
  void _showAbout();
  void _showModifiers();

private:
  Ui::mainForm _ui;
  QWidget *_parentWidget = nullptr;
  GuiWrapperApp &_parentApp;

  void enableStuff();
  void fillData();
  void writeData();
};

class AboutForm : public QDialog {
  Q_OBJECT

public:
  explicit AboutForm(QWidget *parent);

private:
  Ui::aboutDialog _ui;
  QWidget *_parent = nullptr;
};

class ModifiersForm : public QDialog {
  Q_OBJECT

public:
  explicit ModifiersForm(QWidget *parent);

private:
  Ui::modifiersDialog _ui;
  QWidget *_parent = nullptr;
};

} // namespace forms

class GuiWrapperApp {
public:
  GuiWrapperApp();
  ~GuiWrapperApp();

  void show();

  friend class forms::MainForm;

private:
  QWidget *_widget = nullptr;
  forms::MainForm *_mainForm = nullptr;

  QByteArray _fileData;
  QString _filePath;
};

} // namespace pqe

// p3qe
// Copyright (C) 2023-2024 jan Mikowa
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
