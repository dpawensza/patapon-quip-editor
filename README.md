# p3qe β
*A custom quip editor for Patapon 3.*

> **WARNING:** This project is in beta stage and might break. If the version number is less than `1.0.0`, use at your own risk!

[[_TOC_]]

# Installation
So far the only way to install p3qe is to compile it yourself.
I don't use Windows, so compilation instructions are only for Linux as of now.

## Building from source
### Dependencies
- Qt >= 6.6.0

### Compiling
```bash
git clone https://gitlab.com/dpawensza/patapon-quip-editor.git --depth=1 --recursive
cd patapon-quip-editor
qmake6
make
```
The executable is placed under `build/pqe`.

### Installing to PATH
```bash
sudo install -Dm755 build/pqe /usr/bin
```

### Removing from PATH
```bash
sudo rm /usr/bin/pqe
```

# Usage
> **Note on file location**  
> Quips and other configuration data are located in `<memstick>/SAVEDATA/<gameID>_DATA00/SECURE.BIN`, where `<memstick>` is your memstick location and `<gameID>` is the ID of your region's savefile:
> - :flag_eu: EU - `UCES01421`
> - :flag_jp: JP - `NPJG00122`
> - :flag_us: NA - `UCUS98751`
> - :flag_cn: AS - `UCAS40318`

0. Launch p3qe.
1. If you have not decrypted your data manually, check `P3 decrypt`.
2. Press `Load` and select the file that contains your quips.
3. Make any changes you'd like.
4. Enable `P3 encrypt` to re-encrypt your data for ready use in game.
5. Set `Enable backups` to whichever state you want and press `Save`. If p3qe doesn't abruptly close, you've correctly saved your quips!
6. Enjoy the modified data!

# License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.  

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
