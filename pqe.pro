# Setup
TEMPLATE = app
TARGET = pqe
INCLUDEPATH += .
INCLUDEPATH += libP3Hash

# Version
VERSION = 0.2.0-1
DEFINES += VERSION=\"\\\"$${VERSION}\\\"\"

# Qt modules
QT += gui widgets

# Configuration
CONFIG += c++2a strict_c++ qt release optimize_full

# Input
SOURCES += src/main.cpp
SOURCES += src/ui.cpp src/files.cpp src/error.cpp
HEADERS += src/ui.hpp src/files.hpp src/error.hpp
HEADERS += src/constants.hpp

# Forms
FORMS += src/ui/mainwidget.ui
FORMS += src/ui/about.ui
FORMS += src/ui/modifiers.ui

# Output
DESTDIR = build
